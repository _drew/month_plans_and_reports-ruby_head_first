require 'yaml/store'
class MonthReportStore

  def initialize(file_name)
    @store = YAML::Store.new(file_name)
  end

  def find(id)
    @store.transaction do
    @store[id]
    end
  end

  def all
    @store.transaction do
      @store.roots.map{|id| @store[id]}
    end  
  end

  def save(month_report)
    @store.transaction do
      unless month_report.id
        highest_id = @store.roots.max || 0
        month_report.id = highest_id + 1
        end
        @store[month_report.id] = month_report  
    end
  end
end
