require'yaml/store'

class MonthPlanStore

#�������� ��������� ��� ������ � ����
  def initialize(file_name)
    @store = YAML::Store.new(file_name)
    
  end

#���������� ������ �� �����
  def find(id)
    @store.transaction do
      @store[id]
    end
  end
#�������� ��� ������� �� ���������
  def all
    @store.transaction do
	#������ ������ �� ���������� ���� ������
      @store.roots.map {|id| @store[id]}
    end
  end

#���������� ������� month_plan

  def save(month_plan)
    @store.transaction do
	#���� ��� �� �������� �������������,������� ���������� ����,����������� �� 1
      unless month_plan.id  
        highest_id = @store.roots.max || 0
        month_plan.id = highest_id + 1
      end
      	#���������� ������� ��� id
      @store[month_plan.id] = month_plan
    end
  end
end