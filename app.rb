require 'sinatra'
require 'month_plan'
require 'month_plan_store'
require 'month_report'
require 'month_report_store'


#Create storage objects for writing to file.
 
store_month_plan = MonthPlanStore.new('month_plans.yml') 
store_month_report = MonthReportStore.new('month_reports.yml')

#Main page

get '/' do
   erb :main
end

№#Upload all plans  
get '/month_plan' do
  @month_plans = store_month_plan.all
  erb :month_plans_index
end

#Upload all reports  
get '/month_report' do
  @month_reports = store_month_report.all
  erb :month_reports_index
end

#Add new plan
get '/month_plan/new' do
  erb :month_plan_new
end

#Add new report
get '/month_report/new' do
  erb :month_report_new
end


# Get plan data from form and save it to file 
post '/month_plan/create' do
  @month_plan = MonthPlan.new
  @month_plan.month = params['month']
  @month_plan.to_month_plan = params['to_month_plan']
  @month_plan.aks_month_plan = params['aks_month_plan']
  @month_plan.simall_month_plan = params['simall_month_plan']
  @month_plan.simprod_month_plan = params['simprod_month_plan']
  @month_plan.simsmart_month_plan = params['simsmart_month_plan']
  @month_plan.dso_month_plan = params['dso_month_plan']
  @month_plan.dop_month_plan = params['dop_month_plan']
  @month_plan.ap_month_plan = params['ap_month_plan']
  @month_plan.stv_month_plan = params['stv_month_plan']
  @month_plan.md_month_plan = params['md_month_plan']
  @month_plan.mdk_month_plan = params['mdk_month_plan']
  store_month_plan.save(@month_plan)
  redirect '/month_plan/new'
end


# Get report data from form and save it to file 
post '/month_report/create' do
  @month_report = MonthReport.new
  @month_report.month = params['month']
  @month_report.to_month_report = params['to_month_report']
  @month_report.aks_month_report = params['aks_month_report']
  @month_report.simall_month_report = params['simall_month_report']
  @month_report.simprod_month_report = params['simprod_month_report']
  @month_report.simsmart_month_report = params['simsmart_month_report']
  @month_report.dso_month_report = params['dso_month_report']
  @month_report.dop_month_report = params['dop_month_report']
  @month_report.ap_month_report = params['ap_month_report']
  @month_report.stv_month_report = params['stv_month_report']
  @month_report.md_month_report = params['md_month_report']
  @month_report.mdk_month_report = params['mdk_month_report']
  store_month_report.save(@month_report)
  redirect '/month_report/new'
end


#Upload plan from storage by key
get '/month_plan/:id' do

  id = params['id'].to_i
 @month_plan = store_month_plan.find(id)
  erb :month_plan_show

end

#Upload report from storage by key
get '/month_report/:id' do

  id = params['id'].to_i
  @month_report = store_month_report.find(id)
  erb :month_report_show

end